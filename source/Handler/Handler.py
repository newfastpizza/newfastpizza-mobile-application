import kivy
from Comm.AuthComm import AuthComm
from Comm.DriversComm import DriversComm
from Comm.OrdersComm import OrdersComm
from Comm.TownComm import TownComm
from kivy.uix.popup import Popup
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.scrollview import ScrollView
from kivy.uix.textinput import TextInput
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.clock import Clock



class Handler():

    def __init__(self, sm):
        self.sm = sm
        self.acomm = AuthComm()
        self.dcomm = DriversComm()
        self.ocomm = OrdersComm()
        self.tcomm = TownComm()
        self.event = None

    def login(self, log, passw):
        retcode = self.acomm.loginreq(log, passw)
        print(retcode)
        if (retcode == 200):
            Token = self.acomm.getToken()
            print(Token)
            self.loginsuccess(Token)
        elif (retcode == -1):
            popup = Popup(title='Failed auth popup', content=Label(text="Connection error"), size_hint=(0.66, 0.25))
            popup.open()
        else:
            popup = Popup(title='Failed auth popup', content=Label(text="Bad credentials"), size_hint=(None, None), size=(400, 200))
            popup.open()


    def loginsuccess(self, Token):
        retcode = self.dcomm.getdriverreq(Token)
        if (retcode == 200):
            if (self.dcomm.role == 'ROLE_DRIVER'):
                print(self.dcomm.status)
                if (self.dcomm.status == "OUT_OF_OFFICE"):
                    self.openretscreen()
                elif (self.dcomm.status == "FREE"):
                    self.openfreescreen()
                elif (self.dcomm.status == "HAS_ORDER"):
                    self.openorderscreen()
            else:
                popup = Popup(title='Failed auth popup', content=Label(text="This user is not a driver"), size_hint=(0.66, 0.25))
                popup.open()
        elif (retcode == -1):
            popup = Popup(title='Failed auth popup', content=Label(text="Connection error"), size_hint=(0.66, 0.25))
            popup.open()
        else:
            popup = Popup(title='Failed auth popup', content=Label(text="This user is not a driver"), size_hint=(0.66, 0.25))
            popup.open()

    def openretscreen(self):
        self.dcomm.getdriverpos(self.acomm.getToken())
        self.sm.get_screen('rs').ids.locationlabel.text = ("Current location: " + self.dcomm.getPosStreet() + 
        ' street, building ' + self.dcomm.getPosBuilding())
        self.sm.current = 'rs'

    def refreshtimer(self, dt):
        if (self.sm.get_screen('fs').ids.refreshtimer.text == "1"):
            self.sm.get_screen('fs').ids.refreshtimer.text = "30"
            self.refreshfree()
        else:
            self.sm.get_screen('fs').ids.refreshtimer.text = str(int(self.sm.get_screen('fs').ids.refreshtimer.text) - 1)

    def openfreescreen(self):
        self.dcomm.getdriverpos(self.acomm.getToken())
        self.sm.get_screen('fs').ids.locationlabel.text = ("Current location: " + self.dcomm.getPosStreet() + 
        ' street, building ' + self.dcomm.getPosBuilding())
        self.sm.get_screen('fs').ids.refreshtimer.text = "30"
        self.sm.current = 'fs'
        self.event = Clock.schedule_interval(self.refreshtimer, 1)


    def openorderscreen(self):
        self.dcomm.getorderreq(self.acomm.getToken())
        self.sm.get_screen('os').ids.orderadresslabel.text = ('Adress: ' + self.dcomm.getOrderRespBody()["townPoint"]["street"] + 
        ' street, building '  +  self.dcomm.getOrderRespBody()["townPoint"]["building"] + ', flat ' + str(self.dcomm.getOrderRespBody()["flatNumber"]))
        self.sm.get_screen('os').ids.orderbranchadresslabel.text = ('Branch adress: ' + self.dcomm.getOrderRespBody()["office"]["street"] + 
        ' street, building '  +  self.dcomm.getOrderRespBody()["office"]["building"])
        self.tcomm.getShortestPath(self.acomm.getToken(), self.dcomm.getOrderRespBody()["townPoint"]["id"], self.dcomm.getOrderRespBody()["office"]["id"])
        print(self.tcomm.getPathResBody())
        laststreet = ""
        firstbuilding = ""
        lastbuilding = ""
        bestpathstring = ""
        for i in self.tcomm.getPathResBody()["path"]:
            if (i["street"]!= laststreet):
                if (laststreet != "" and lastbuilding != ""):
                    if (lastbuilding != firstbuilding):
                        bestpathstring = bestpathstring + "Street " + laststreet + ": from building " + firstbuilding + " to building " + lastbuilding + "\n"
                    else:
                        bestpathstring = bestpathstring + "Street " + laststreet + ", single building " + firstbuilding + "\n"
                firstbuilding = i["building"]
                lastbuilding = i["building"]
                laststreet = i["street"]
            else:
                lastbuilding = i["building"]
        if (lastbuilding != firstbuilding):
            bestpathstring = bestpathstring + "Street " + laststreet + ": from building " + firstbuilding + " to building " + lastbuilding + "\n"
        else:
            bestpathstring = bestpathstring + "Street " + laststreet + ", single building " + firstbuilding + "\n"
        self.sm.get_screen('os').ids.pathwidget.ids.bestpathlabel.text = bestpathstring
        self.sm.current = 'os'
        

    def passwordchange(self, confirmPassword, oldPassword, Password):
        retcode = self.acomm.passwordchangereq(confirmPassword, oldPassword, Password)
        if (retcode == 200):
            popup = Popup(title='Success', content=Label(text="Password changes are successful"), size_hint=(0.66, 0.25))
            popup.open()
        elif (retcode == -1):
            self.logout()
            popup = Popup(title='Failed auth popup', content=Label(text="Connection error"), size_hint=(0.66, 0.25))
            popup.open()
        else: 
            popup = Popup(title='Failed auth popup', content=Label(text="Bad input"), size_hint=(0.66, 0.25))
            popup.open()

    def logout(self):
        self.dcomm.getdriverreq(self.acomm.getToken())
        if (self.dcomm.getStatus() == "FREE"):
            self.dcomm.setdriverstatus(self.acomm.Token, '"OUT_OF_OFFICE"')
        print(self.dcomm.code)
        self.dcomm.Clear()
        self.acomm.Clear()
        self.ocomm.Clear()
        self.tcomm.Clear()
        self.sm.current = 'ls'
        if (self.event):
            self.event.cancel()

    def gotodriver(self):
        self.sm.get_screen('ds').ids.idlabel.text = 'ID: ' + self.dcomm.getID()
        self.sm.get_screen('ds').ids.loginlabel.text = 'Login: ' + self.dcomm.getUsername()
        self.sm.get_screen('ds').ids.namelabel.text = 'Name: ' + self.dcomm.getName()
        self.sm.get_screen('ds').ids.statuslabel.text = 'Status: ' + self.dcomm.getStatus()
        self.sm.get_screen('ds').ids.rolelabel.text = 'Role: ' + self.dcomm.getRole()
        self.sm.get_screen('ds').ids.phonelabel.text = 'Phone: ' + self.dcomm.getPhone()
        self.sm.current = 'ds'
        if (self.event):
            self.event.cancel()

    def returnfromdriver(self):
        retcode = self.dcomm.getdriverreq(self.acomm.Token)
        if (retcode == 200):
            if (self.dcomm.role == 'ROLE_DRIVER'):
                print(self.dcomm.status)
                if (self.dcomm.status == "OUT_OF_OFFICE"):
                    self.openretscreen()
                elif (self.dcomm.status == "FREE"):
                    self.openfreescreen()
                elif (self.dcomm.status == "HAS_ORDER"):
                    self.openorderscreen()
            else:
                popup = Popup(title='Failed auth popup', content=Label(text="Authentication failed"), size_hint=(None, None), size=(400, 200))
                popup.open()
        else:
            self.logout()
            popup = Popup(title='Failed auth popup', content=Label(text="Authentication failed"), size_hint=(None, None), size=(400, 200))
            popup.open()


    def gotoreturn(self):
        retcode = self.dcomm.setdriverstatus(self.acomm.Token, '"OUT_OF_OFFICE"')
        if (self.event):
            self.event.cancel()
        if (retcode == 200):
            self.openretscreen()
        else:
            self.logout()
            popup = Popup(title='Connection error', content=Label(text="Connection error"), size_hint=(None, None), size=(400, 200))
            popup.open()


    def gotoorder(self):
        pass

    def gotofree(self):
        retcode = self.dcomm.setdriverstatus(self.acomm.Token, '"FREE"')
        if (retcode == 200):
                self.openfreescreen()
        else:
            self.logout()
            popup = Popup(title='Connection error', content=Label(text="Connection error"), size_hint=(None, None), size=(400, 200))
            popup.open()

    def refreshfree(self):
        retcode = self.dcomm.getdriverreq(self.acomm.getToken())
        if (retcode == 200):
            if (self.dcomm.role == 'ROLE_DRIVER'):

                if (self.dcomm.status == "FREE"):
                    if (self.event):
                        self.event.cancel()
                    self.openfreescreen()
                elif (self.dcomm.status == "HAS_ORDER"):
                    if (self.event):
                        self.event.cancel()
                    self.openorderscreen()
            else:
                self.logout()
                popup = Popup(title='Connection error', content=Label(text='Connection error'), size_hint=(0.66, 0.25))
                popup.open()
        else:
            self.logout()
            popup = Popup(title='Connection error', content=Label(text='Connection error'), size_hint=(0.66, 0.25))
            popup.open()

    def fullorderinfo(self):
        popupcontent = BoxLayout(orientation = 'vertical')
        productlist = GridLayout(cols = 9)
        scrollview = ScrollView(do_scroll_x = True, do_scroll_y =False)

        productlist.add_widget(Label(text = "Product name"))
        productlist.add_widget(Label(text = "|", size_hint = [0.1, 1]))
        productlist.add_widget(Label(text = "Size/volume"))
        productlist.add_widget(Label(text = "|", size_hint = [0.1, 1]))
        productlist.add_widget(Label(text = "Price per item"))
        productlist.add_widget(Label(text = "|", size_hint = [0.1, 1]))
        productlist.add_widget(Label(text = "Count"))
        productlist.add_widget(Label(text = "|", size_hint = [0.1, 1]))
        productlist.add_widget(Label(text = "Total"))

        for i in self.dcomm.getOrderRespBody()["backet"]:
            productlist.add_widget(Label(text = i["product"]["name"]))
            productlist.add_widget(Label(text = "|", size_hint = [0.1, 1]))
            print(i["product"])
            if ("size" in i["product"]):
                productlist.add_widget(Label(text = i["product"]["size"]))
            elif ("volume" in i["product"]):
                productlist.add_widget(Label(text = str(i["product"]["volume"])))
            else: 
                productlist.add_widget(Label(text = ""))
            productlist.add_widget(Label(text = "|", size_hint = [0.1, 1]))
            productlist.add_widget(Label(text = str(i["pricePerItem"])))
            productlist.add_widget(Label(text = "|", size_hint = [0.1, 1]))
            productlist.add_widget(Label(text = str(i["count"])))
            productlist.add_widget(Label(text = "|", size_hint = [0.1, 1]))
            productlist.add_widget(Label(text = str(i["price"])))


        popupcontent.add_widget(Label(text = "ID: "+ self.dcomm.getOrderRespBody()["id"]))
        popupcontent.add_widget(Label(text = "Products: "))

        scrollview.add_widget(productlist)
        popupcontent.add_widget(scrollview)

        popupcontent.add_widget(Label(text = "Adress: " + self.dcomm.getOrderRespBody()["townPoint"]["street"] + 
        ' street, building '  +  self.dcomm.getOrderRespBody()["townPoint"]["building"] + ', flat ' + str(self.dcomm.getOrderRespBody()["flatNumber"])))
        popupcontent.add_widget(Label(text = "Branch adress: "+ self.dcomm.getOrderRespBody()["office"]["street"] + 
        ' street, building '  +  self.dcomm.getOrderRespBody()["office"]["building"]))
        popupcontent.add_widget(Label(text = "Client's phone number: " + self.dcomm.getOrderRespBody()["phone"]))
        popupcontent.add_widget(Label(text = "Date: " + self.dcomm.getOrderRespBody()["date"]))
        popupcontent.add_widget(Label(text = "Comment: " + self.dcomm.getOrderRespBody()["comment"]))
        popup = Popup(title='Full order info', content=popupcontent, size_hint=(0.9, 0.75))
        popup.open()

    def orderdelivered(self, flag):
        retcode1 = self.dcomm.getorderreq(self.acomm.getToken())
        if (retcode1 == 200):
            retcode2 = self.ocomm.finishOrder(self.acomm.getToken(), self.dcomm.getOrderRespBody()["id"])
            if (retcode2 == 200):
                if (flag == True):
                    self.gotoreturn()
                else:
                    self.gotofree()
            else:
                self.logout()
                popup = Popup(title='Connection error', content=Label(text="Connection error1"), size_hint=(None, None), size=(400, 200))
                popup.open()
        else:
            self.logout()
            popup = Popup(title='Connection error', content=Label(text="Connection error2"), size_hint=(None, None), size=(400, 200))
            popup.open()

    def changeloc(self, fromscreen):
        retcode1 = self.tcomm.getAllPoints(self.acomm.getToken())
        if (retcode1 == 200):
#            streetlist = {point["street"] for point in self.tcomm.getPointsResBody()}

            popupcontent = BoxLayout(orientation = 'vertical', spacing = 35, padding = 20)

            popupgrid = GridLayout(cols = 2, spacing = 35)
            popupgrid.add_widget(Label(text = "Select street: "))
            streetw = TextInput()
            popupgrid.add_widget(streetw)

            popupgrid.add_widget(Label(text = "Select building: "))
            buildingw = TextInput()
            popupgrid.add_widget(buildingw)

            popupcontent.add_widget(popupgrid)
            confbutton = Button(text = "Confirm") 
            buttoncallback = lambda event:self.confirmchangeloc(streetw.text, buildingw.text, fromscreen)
            confbutton.bind(on_press = buttoncallback)
            confbutton.bind(on_press = lambda event: popup.dismiss())
            popupcontent.add_widget(confbutton)
            popup = Popup(title='Changing location', content=popupcontent, size_hint=(0.7, 0.5))
            popup.open()



        else:
            self.logout()
            popup = Popup(title='Connection error', content=Label(text="Connection error"), size_hint=(None, None), size=(400, 200))
            popup.open()

    def confirmchangeloc(self, street, building, fromscreen):
        found = False
        pointid = ""
        for point in self.tcomm.getPointsResBody():
            if (point["street"] == street and point["building"] == building ):
                found = True
                pointid = point["id"]
        if (found == True):
            retcode = self.dcomm.setdriverpos(self.acomm.getToken(), pointid)
            if (retcode != 200):
                popup = Popup(title='Connection error', content=Label(text="Something went wrong"), size_hint=(None, None), size=(400, 200))
                popup.open()
            else:
                if (fromscreen == "FREE"):
                    self.refreshfree()
                else:
                    self.gotoreturn()
        else:
            popup = Popup(title='Bad input', content=Label(text="This adress doesnt exist inside our database"), size_hint=(None, None), size=(400, 200))
            popup.open()


