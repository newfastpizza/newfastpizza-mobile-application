import kivy
import requests
from Handler.Handler import Handler


from kivy.config import Config
Config.set('graphics', 'resizable', 0)
Config.set('graphics', 'width', 600)
Config.set('graphics', 'height', 800)
Config.set('kivy', 'keyboard_mode', 'systemanddock')
Config.write()

from kivy.app import App

from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.core.window import Window
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label

from kivy.network.urlrequest import UrlRequest

Builder.load_file('Screens/LoginScreen.kv')
Builder.load_file('Screens/PathWidget.kv')
Builder.load_file('Screens/MenuWidget.kv')
Builder.load_file('Screens/DMenuWidget.kv')
Builder.load_file('Screens/FreeScreen.kv')
Builder.load_file('Screens/OrderScreen.kv')
Builder.load_file('Screens/ReturnScreen.kv')
Builder.load_file('Screens/DriverScreen.kv')


class LoginScreen(Screen):
    pass

class DriverScreen(Screen):
    pass

class FreeScreen(Screen):
    pass

class OrderScreen(Screen):
    pass

class ReturnScreen(Screen):
    pass

sm = ScreenManager()
sm.add_widget(LoginScreen(name = 'ls'))
sm.add_widget(DriverScreen(name = 'ds'))
sm.add_widget(FreeScreen(name = 'fs'))
sm.add_widget(OrderScreen(name = 'os'))
sm.add_widget(ReturnScreen(name = 'rs'))

sm.current = 'ls'




class main_kv(BoxLayout):
    pass




class MainApp(App):
    handlr = Handler(sm)
    def build(self):
        self.x = 600
        self.y = 800
        return sm 


        



if __name__ == '__main__':
    MainApp().run()