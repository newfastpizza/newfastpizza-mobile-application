import requests
import configparser

class TownComm():

    
    def __init__(self):
        self.code = 999   
        self.posid = ""
        self.posstreet = ""
        self.posbuilding = ""
        self.isposbranch = True
        self.pointsresbody = ""
        self.branchesresbody = ""
        self.pathresbody = ""
        self.config = configparser.ConfigParser()  
        self.config.read("Config/settings.ini")  

    def getCode(self):
        return self.code

    def getPosID(self):
        return self.posid
    
    def getPosStreet(self):
        return self.posstreet

    def getPosBuilding(self):
        return self.posbuilding
        
    def getIsPosBranch(self):
        return self.isposbranch
        
    def getPointsResBody(self):
        return self.pointsresbody
        
    def getBranchResBody(self):
        return self.branchesresbody
        
    def getPathResBody(self):
        return self.pathresbody

    def Clear(self):
        self.code = 999   
        self.posid = ""
        self.posstreet = ""
        self.posbuilding = ""
        self.isposbranch = True
        self.pointsresbody = ""
        self.branchesresbody = ""
        self.pathresbody = ""


    def getPointbyID(self, token, posid):
        headers = {'accept': 'application/json', 'Authorization': token}
        try:
            response = requests.get(self.config["configs"]["serverURL"] + 'api/town/' + posid , headers=headers)
        except:
            self.code = -1
        else:
            self.code = response.status_code
            if (response and self.code == 200):
                self.posid = response.json()["id"]
                self.posstreet = response.json()["street"]
                self.posbuilding = response.json()["building"]
                self.isposbranch = response.json()["IsBranchOffice"]
        finally:
            return self.code

    def getAllPoints(self, token):
        headers = {'accept': 'application/json', 'Authorization': token}
        try:
            response = requests.get(self.config["configs"]["serverURL"] + 'api/town/', headers=headers)
        except:
            self.code = -1
        else:
            self.code = response.status_code
            if (response and self.code == 200):
                self.pointsresbody = response.json()
        finally:
            return self.code

    def getAllBranches(self):
        headers = {'accept': 'application/json'}
        try:
            response = requests.get(self.config["configs"]["serverURL"] + 'api/town/offices/', headers=headers)
        except:
            self.code = -1
        else:
            self.code = response.status_code
            if (response and self.code == 200):
                self.branchesresbody = response.json()
        finally:
            return self.code

    def getShortestPath(self, token, posid1, posid2):
        headers = {'accept': 'application/json', 'Authorization': token}
        try:
            response = requests.get(self.config["configs"]["serverURL"] + 'api/town/path/' + posid1 + '/' + posid2, headers=headers)
        except:
            self.code = -1
        else:
            self.code = response.status_code
            if (response and self.code == 200):
                self.pathresbody = response.json()
        finally:
            return self.code
