import requests
import configparser

class OrdersComm():

    
    def __init__(self):
        self.code = 999   
        self.currentorder = ""
        self.ordersbody = ""
        self.config = configparser.ConfigParser()  
        self.config.read("Config/settings.ini")  

    def getCode(self):
        return self.code

    def getCurrentOrder(self):
        return self.currentorder

    def getOrdersBody(self):
        return self.ordersbody

    def Clear(self):
        self.code = 999   
        self.currentorder = ""
        self.ordersbody = ""

    def getCurrentOrder(self, token, orderid):
        headers = {'accept': 'application/json', 'Authorization': token}
        try:
            response = requests.get(self.config["configs"]["serverURL"] + 'api/orders/' + orderid, headers=headers)
        except:
            self.code = -1
        else:
            self.code = response.status_code
            if (response and self.code == 200):
                self.currentorder = response.json()
        finally:
            return self.code



    def getOrders(self, token): 
        headers = {'accept': 'application/json', 'Authorization': token}
        try:
            response = requests.get(self.config["configs"]["serverURL"] + 'api/orders', headers=headers)
        except:
            self.code = -1
        else:
            self.code = response.status_code
            if (response and self.code == 200):
                self.ordersbody = response.json()
        finally:
            return self.code

    def finishOrder(self, token, orderid):
        headers = {'accept': 'application/json', 'Authorization': token}
        print(orderid)
        try:
            response = requests.get(self.config["configs"]["serverURL"] + 'api/orders/' + orderid + '/finish', headers=headers)
        except : 
            self.code = -1
        else:
            self.code = response.status_code
        finally:
            print (self.code)
            return self.code
