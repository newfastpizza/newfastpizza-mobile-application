import requests
import configparser

class AuthComm():

    def __init__(self):
        self.code = 999
        self.Token = ""
        self.login = ""
        self.passw = ""
        self.config = configparser.ConfigParser()  
        self.config.read("Config/settings.ini")  

    def getToken(self):
        return self.Token

    def getCode(self):
        return self.code

    def Clear(self):
        self.code = 999
        self.Token = ""

    def loginreq(self, log, passw):
        self.log = log
        self.passw = passw
        headers = {'accept': '*/*', 'Content-Type': 'application/json'}
        data = '{ "password": "' + passw + '", "username": "' + log + '"}'
        try:
            response = requests.post(self.config["configs"]["serverURL"] + "api/auth/signin", headers=headers, data=data)
        except:
            self.code = -1
        else:
            self.code = response.status_code
            if (self.code == 200):
                self.Token = "Bearer "+ response.text
        finally:
            return self.code

    def revalidatetoken(self):
        headers = {'accept': '*/*', 'Content-Type': 'application/json'}
        data = '{ "password": "' + self.passw + '", "username": "' + self.log + '"}'
        try:
            response = requests.post(self.config["configs"]["serverURL"] + "api/auth/signin", headers=headers, data=data)
        except:
            self.code = -1
        else:
            self.code = response.status_code
            if (self.code == 200):
                self.Token = "Bearer "+ response.text
        finally:
            return self.code


    def passwordchangereq(self, confirmPassword, oldPassword, Password):
        headers = {'accept': '*/*', 'Authorization': self.Token, 'Content-Type': 'application/json'}
        data = '{ "confirmPassword": "' + confirmPassword +'", "oldPassword": "'+ oldPassword +'", "password": "'+Password+'"}'
        try:
            response = requests.post(self.config["configs"]["serverURL"] + "api/auth/password", headers=headers, data=data)
        except:
            self.code = -1
        else:
            self.code = response.status_code
        finally:
            return self.code


