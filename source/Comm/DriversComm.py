import requests
import configparser


class DriversComm():
    
    def __init__(self):
        self.code = 999   
        self.id = ""
        self.username = ""
        self.role = ""
        self.name = ""
        self.phone = ""
        self.status = ""
        self.posid = ""
        self.posstreet = ""
        self.posbuilding = ""
        self.isposbranch = True
        self.orderresponsebody = ""
        self.config = configparser.ConfigParser()  
        self.config.read("Config/settings.ini")  

    def getCode(self):
        return self.code

    def getID(self):
        return self.id

    def getUsername(self):
        return self.username

    def getRole(self):
        return self.role

    def getName(self):
        return self.name

    def getPhone(self):
        return self.phone

    def getStatus(self):
        return self.status

    def getPosID(self):
        return self.posid

    def getPosStreet(self):
        return self.posstreet

    def getPosBuilding(self):
        return self.posbuilding

    def getIsPosBranch(self):
        return self.isposbranch

    def getOrderRespBody(self):
        return self.orderresponsebody

    def Clear(self):
        self.code = 999   
        self.id = ""
        self.username = ""
        self.role = ""
        self.name = ""
        self.phone = ""
        self.status = ""
        self.posid = ""
        self.posstreet = ""
        self.posbuilding = ""
        self.isposbranch = True
        self.orderresponsebody = ""

    def getdriverreq(self, token):
        headers = {'accept': 'application/json', 
        'Authorization': token}
        try:
            response = requests.get(self.config["configs"]["serverURL"] + "api/drivers/profile", headers=headers)
        except:
            self.code = -1
        else:
            self.code = response.status_code
            if (response and self.code == 200):
                print(response.text)
                self.id = response.json()["id"]
                self.username = response.json()["username"]
                self.role = (response.json()["role"])["roleType"]
                self.name = response.json()["name"]
                self.phone = response.json()["phone"]
                self.status = response.json()["status"]
        finally:
            return self.code

    def getorderreq(self, token):
        headers = {
        'accept': 'application/json', 'Authorization': token}
        try:
            response = requests.get(self.config["configs"]["serverURL"] + 'api/drivers/' + self.id + '/order', headers=headers)
        except:
            self.code = -1
        else:
            self.code = response.status_code
            if (response and self.code == 200):
                self.orderresponsebody = response.json()
        finally:
            return self.code

    def getdriverpos(self, token):
        headers = {
        'accept': 'application/json', 'Authorization': token}
        try:
            response = requests.get(self.config["configs"]["serverURL"] + 'api/drivers/' + self.id + '/position', headers=headers)
        except:
            self.code = -1
        else:
            self.code = response.status_code
            if (response and self.code == 200):
                self.posid = response.json()["id"]
                self.posstreet = response.json()["street"]
                self.posbuilding = response.json()["building"]
                self.isposbranch = response.json()["isBranchOffice"]
        finally:
            return self.code
    
    def setdriverpos(self, token, posid):
        headers = {'accept': 'application/json', 'Authorization': token, 'Content-Type': 'application/json'}
        data = posid
        try:
            response = requests.post(self.config["configs"]["serverURL"] + 'api/drivers/' + self.id + '/position', headers=headers, data=data)
        except:
            self.code = -1
        else:
            self.code = response.status_code
            if (response and self.code == 200):
                self.posid = response.json()["id"]
                self.posstreet = response.json()["street"]
                self.posbuilding = response.json()["building"]
                self.isposbranch = response.json()["isBranchOffice"]
        finally:
            return self.code


    def setdriverstatus(self, token, status):
        headers = {'accept': 'application/json', 'Authorization': token,'Content-Type': 'application/json',}
        data = status 
        try:
            response = requests.post(self.config["configs"]["serverURL"] + 'api/drivers/' + self.id + '/status', headers=headers, data=data)
        except:
            self.code = -1
        else:
            self.code = response.status_code
            if (response and self.code == 200):
                self.status = status
        finally: 
            return self.code